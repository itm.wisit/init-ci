<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('cms_model');
  }

  
	public function index()
	{
    $this->load->view('template/header');
    $this->load->view('cms/main');
    $this->load->view('template/footer');
  }

  public function edit() {
    $id = 1;
    $profile = $this->cms_model->get_profile($id);
    $data['profile'] = $profile;
    $this->load->view('template/header');
    $this->load->view('cms/edit', $data);
    $this->load->view('template/footer');
  }
  public function clearValue($array) {
    unset($array['response_sive']);
    unset($array['photo_path']);
    unset($array['x1']);
    unset($array['y1']);
    unset($array['x2']);
    unset($array['y2']);
    unset($array['w']);
    unset($array['h']);
    unset($array['w_div']);
    unset($array['h_div']);
    return $array;
  }
  public function submit_edit() {
    $params = $this->input->post();
    if (!array_key_exists('id', $params)) {
      $insert = $this->clearValue($params);

      $params['id'] = $this->cms_model->insert_data($insert, 'clinic_profile');
    } else {
      $update = $this->clearValue($params);
      unset($update['id']);
      $updated = $this->cms_model->update_data($update, 'clinic_profile', array('id' => $params['id']));
    }
    if(!empty($params['photo_path'][0])){
			$new_upload = $this->upload_highlight_banner($_FILES['photo'], $params);
    } 
    if(!empty($new_upload[0])) {
      $updated = $this->cms_model->update_data($new_upload[0], 'clinic_profile', array('id' => $params['id']));
    }
		redirect('cms/index');
  }

  function set_upload_options($banner_id){
    $config = array();
    $config['upload_path'] = FCPATH.'fileadmin/upload_img/Highlight_banner/'.$banner_id.'/original/';
    $config['allowed_types'] = 'gif|jpeg|jpg|pjpeg|png|x-png';
    $config['max_width']  = '0';
    $config['max_height']  = '0';

    return $config;
  }
  function upload_highlight_banner($files, $data){ 
    $photo = $_FILES['photo'];
    $default_path = FCPATH.'fileadmin/upload_img/Highlight_banner/'.$data['id'];
    $default_mobile_path = FCPATH.'fileadmin/upload_img/Highlight_banner/'.$data['id'].'/mobile';
    $upload_mobile_path = FCPATH.'fileadmin/upload_img/Highlight_banner/'.$data['id'].'/mobile/original/';
    $config = $this->set_upload_options($data['id']);
    $new_data = array();
    if(!is_dir($default_path)){
       mkdir($default_path, 0777, TRUE);
    }
    if(!is_dir($config['upload_path'])){
       mkdir($config['upload_path'], 0777, TRUE);
    }
    if(!is_dir($default_mobile_path)){
       mkdir($default_mobile_path, 0777, TRUE);
    }
    if(!is_dir($upload_mobile_path)){
       mkdir($upload_mobile_path, 0777, TRUE);
    }

     
    $this->load->library('upload',$config);
    for($i=0;$i<count($photo['name']);$i++){
      if($data['response_sive']['desktop'] == FALSE && $data['response_sive']['mobile'] == TRUE){
        ++$i;
      }

      if($photo['name'][$i] != ''){
        $_FILES['userfile']['name']= $photo['name'][$i];
        $_FILES['userfile']['type']= $photo['type'][$i];
        $_FILES['userfile']['tmp_name']= $photo['tmp_name'][$i];
        $_FILES['userfile']['error']= $photo['error'][$i];
        $_FILES['userfile']['size']= $photo['size'][$i];    					

        $new_name = $this->replace_upload_file_name(time().'_'.$_FILES['userfile']['name']);
        $config['file_name'] = $new_name;
        $upload_path = $config['upload_path'];
        if($i == 1){
          $upload_path = $upload_mobile_path;
          $config['upload_path'] = $upload_mobile_path;
          $default_path = $default_mobile_path;
        }

        $this->upload->initialize($config);
        if ( !$this->upload->do_upload('userfile') ){
          $error = array('error' => $this->upload->display_errors());
        }else{
          $finfo = $this->upload->data();
          // print_r($finfo);
        
          $new_data[$i] = $this->resize_highlight_banner($finfo, $data, $upload_path, $default_path, $i);
          
        }
      }
    }
    return $new_data;
  }
  	function resize_highlight_banner($finfo, $data, $upload_path, $default_path, $index){ 
		$data['width'] =  450;
		$data['height'] =  300;
		$origin_w = $finfo['image_width'];
		$origin_h = $finfo['image_height'];
		$ratio_w = (int)$origin_w/(int)$data['w_div'][$index];
		$ratio_h = (int)$origin_h/(int)$data['h_div'][$index];
	
		 $path = $upload_path.$finfo['file_name'];
		//  echo $path;
		 // print_r($path);die();
		 @chmod($path, 0644);
		//////// crop Picture ////////////
		$aSize = getimagesize($path);
		$crop_path = $default_path."/".$finfo['file_name'];
		// echo $crop_path;
		if($aSize[2] == '2'){
			$vImg = @imagecreatefromjpeg($path);
		}else if($aSize[2] =='3'){
			 $vImg = @imagecreatefrompng($path);
		}

		if($data['w'][$index]==0  || $data['h'][$index]==0  ){
			$vDstImg = @imagecreatetruecolor( $origin_w, $origin_h );
			imagecopyresampled($vDstImg, $vImg, 0, 0, (int)($data['x1'][$index]=0)*$ratio_w, (int)($data['y1'][$index]=0)*$ratio_h, $origin_w, $origin_h , $origin_w,  $origin_h );
		} else {
			$vDstImg = @imagecreatetruecolor( $origin_w, ceil((int)($origin_w*$data['height'])/$data['width']) );
			imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$data['x1'][$index]*$ratio_w, (int)$data['y1'][$index]*$ratio_h, $origin_w, ceil((int)($origin_w*$data['height'])/$data['width']), (int)$data['w'][$index]*$ratio_w, (int)$data['h'][$index]*$ratio_h);
		}

		imagejpeg($vDstImg, $crop_path, 100); 
		$new_data['banner_image_path'] = $finfo['file_name']; 	
		return $new_data;
	}

  function replace_upload_file_name($str){
    $str = trim($str);
    $str = str_replace(' ','_',$str);
    // $str = preg_replace('/[ก-เ\ ]/',"",$str);
    return $str;
  }
  public function upload() {
    $this->load->view('template/header');
    $this->load->view('cms/upload');
    $this->load->view('template/footer');
  }
}
?>