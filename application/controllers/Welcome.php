<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function test()
	{
		$this->load->view('welcome_message');
	}

	public function index()
	{
		$seats = 15;
		$vote = [
			'A' => 15000, 
			'B' => 5400, 
			'C' => 5500,
			'D' => 5550
		];
		$total = array_sum($vote);
		$vote_per_seats = $total / $seats;
		$result = array();
		foreach($vote as $key => $value ) {
			$res = substr(round($value / $vote_per_seats, 2), 0, -1);
			$result[$key] = round($res, 0, PHP_ROUND_HALF_DOWN);
		}
		print_r($result);
		echo '<hr>';
		$hello = 'test';
		$a = "hello";
		echo $$a;
 		// $this->load->view('welcome_message');
	}

}
