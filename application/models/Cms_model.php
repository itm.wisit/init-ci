<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Cms_model extends CI_Model {
    public function __construct() {
    //   parent::__construct();
      $this->load->database();
    }
    
    public function get_profile($id){
      $this->db->select('*');
      $this->db->where('id', $id);
      $result = $this->db->get('clinic_profile');
      return $result->row_array();
    }

	function insert_data($array, $table) {
    $this->db->set($array);
    $data = $this->db->insert($table);
    return $this->db->insert_id();
  } 
  function update_data($array, $table, $array_where) {
    $this->db->where($array_where);
    $this->db->update($table, $array);
    return $this->db->affected_rows();
  }

}
