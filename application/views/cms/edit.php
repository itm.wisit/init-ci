<div class="container mt-5">
  <div class="row">
    <div class="col-12">
      <div class="d-flex justify-content-center">
        <h1>ข้อมูลสถานประกอบการ</h1>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <form method="POST" action="<?php echo site_url('cms/submit_edit');?>" enctype="multipart/form-data">
        <?php if(!empty($profile)) { ?> 
        <input type="hidden" id="id" name="id" value="<?php echo $profile['id'];?>">
        <?php }?>
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label for="name_th">ชื่อคลีนิค</label>
              <input class="form-control" id="name_th" type="text" name="name_th" value="<?php echo $profile['name_th'];?>" placeholder="" />
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="name_en">clinic Name</label>
              <input class="form-control" id="name_en" type="text" name="name_en" value="<?php echo $profile['name_en'];?>" placeholder="" />
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="form-group">
              <label for="licence_no">เลขที่ใบอนุญาตประกอบสถานพยาบาล</label>
              <input class="form-control" id="licence_no" type="text" name="licence_no" value="<?php echo $profile['licence_no'];?>" placeholder="" />
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="form-group">
              <label for="licence_date">วันหมดอายุใบอนุญาต</label>
              <input class="form-control" id="licence_date" type="text" name="licence_date" value="<?php echo $profile['licence_date'];?>" placeholder="" />
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="form-group">
              <label for="address">ที่อยู่</label>
              <input class="form-control" id="address" type="text" name="address" value="<?php echo $profile['address'];?>" placeholder="" />
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="form-group">
              <label for="owner_name">ชื่อผู้ประกอบกิจการสถานพยาบาล</label>
              <input class="form-control" id="owner_name" type="text" name="owner_name" value="<?php echo $profile['owner_name'];?>"  placeholder="" />
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="form-group">
              <label for="operation_name">ชื่อผู้ดำเนินการสถานพยาบาล</label>
              <input class="form-control" id="operation_name" type="text" name="operation_name" value="<?php echo $profile['operation_name'];?>" placeholder="" />
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="form-group">
              <label for="open_date">เปิดให้บริการครั้งแรกเมื่อ</label>
              <input class="form-control" id="open_date" type="text" name="open_date" value="<?php echo $profile['open_date'];?>" placeholder="" />
            </div>
          </div>
        </div>
        <div class="row mt-5">
          <div class="col-12">
            <h2>ข้อมูลติดต่อ</h2>
          </div>
        </div>
        <div class="row mt-2">
          <div class="col-6">
            <div class="form-group">
              <label for="tel">เบอร์โทรศัพท์</label>
              <input class="form-control" id="tel" type="text" name="tel" value="<?php echo $profile['tel'];?>" placeholder="" />
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="email">Email</label>
              <input class="form-control" id="email" type="email" name="email" value="<?php echo $profile['email'];?>" placeholder="" />
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label for="website">เว็บไซต์</label>
              <input class="form-control" id="website" type="text" name="website" value="<?php echo $profile['website'];?>" placeholder="" />
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="social_media">Social Media</label>
              <input class="form-control" id="social_media" type="text" name="social_media" value="<?php echo $profile['social_media'];?>" placeholder="" />
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label class="bmd-label-static" for="type">สิทธิ์การรักษาพยาบาล</label>
              <select class="custom-select custom-select-lg mb-3" name="type">
                <option selected="">เลือกสิทธิ์การรักษาพยาบาล</option>
                <option value="1" <?php echo ( $profile['type'] == 1) ? 'selected="selected"' : ''?> >One</option>
                <option value="2" <?php echo ( $profile['type'] == 2) ? 'selected="selected"' : ''?> >Two</option>
                <option value="3" <?php echo ( $profile['type'] == 3) ? 'selected="selected"' : ''?> >Three</option>
              </select>
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label class="bmd-label-static" for="payment">การชำระค่ารักษาพยาบาล</label>
              <select class="custom-select custom-select-lg mb-3" name="payment">
                <option selected="">เลือกการชำระค่ารักษาพยาบาล</option>
                <option value="1" <?php echo ( $profile['payment'] == 1) ? 'selected="selected"' : ''?>>One</option>
                <option value="2" <?php echo ( $profile['payment'] == 2) ? 'selected="selected"' : ''?>>Two</option>
                <option value="3" <?php echo ( $profile['payment'] == 3) ? 'selected="selected"' : ''?>>Three</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label for="waiting_time">ระยะเวลารอคิวโดยประมาณ</label>
              <input class="form-control" id="waiting_time" type="text" name="waiting_time" value="<?php echo $profile['waiting_time'];?>" placeholder="" />
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="payment_rates">ค่าบริการการแพทย์เริ่มต้นที่</label>
              <input class="form-control" id="payment_rates" type="text" name="payment_rates" value="<?php echo $profile['payment_rates'];?>" placeholder="" />
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <label class="bmd-label-static" for="place_at">แพทย์เข้าตรวจที่</label>
              <select class="custom-select custom-select-lg mb-3" name="place_at">
                <option selected=""></option>
                <option value="1" <?php echo ( $profile['place_at'] == 1) ? 'selected="selected"' : ''?>>One</option>
                <option value="2" <?php echo ( $profile['place_at'] == 2) ? 'selected="selected"' : ''?>>Two</option>
                <option value="3" <?php echo ( $profile['place_at'] == 3) ? 'selected="selected"' : ''?>>Three</option>
              </select>
          </div>
        </div>

        <div class="row mt-5">
          <div class="col-12">
            <h2>อัพโหลดรูปภาพ</h2>
          </div>
        </div>
        <div class="row mb-5">
          <div class="col-12">
            <label class="bmd-label-static" for="upload">Images</label>
          </div>
          <div class="col-12">
            <?php 
              $config['width'] = '450';
              $config['height'] = '300';
              $config['id'] = 'desktop';
              $config['name'] = 'photo[]';
              $config['init'] = 1;
              $config['photo'] = (isset($profile) ? $profile['banner_image_path'] : '');
              $config['pic_path'] = 'fileadmin/upload_img/Highlight_banner/'.(isset($profile) ? $profile['id'] : 'false').'/';

              $this->load->view('cms/template/Jcrop_Picture_Multi_Size', $config);
            ?>
          </div>                                                  
        </div>
        <button class="btn btn-primary" type="submit">Submit</button>
      </form>
    </div>
  </div>
</div>