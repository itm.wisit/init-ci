
<input type="file" name='<?php echo $name ?>' id='<?php echo $id ?>' onchange="add_namepath(this,'<?php echo $id ?>')" accept="image/*">  
<div class="img-preview-note">
	* Image uploaded can be no longer than 1MB and it will be cropped in <?php echo $width; ?> x <?php echo $height; ?> px
</div>                                                 
<div id="div_preview" class="img-preview">
		<input type="hidden" name="response_sive[<?php echo $id ?>]" id="photo_size_<?php echo $id ?>">
		<input class="nomargin" type="hidden" name="photo_path[]" value="" placeholder="" id="photo_path_<?php echo $id ?>">
    <img class ='imagePreviewLarge col-8 ' alt="" id="preview_<?php echo $id ?>" src="<?php echo $photo ? base_url().$pic_path.$photo : 'https://via.placeholder.com/450x300';?>">
		<input type="hidden" id="x1_<?php echo $id ?>" name="x1[]" />
		<input type="hidden" id="y1_<?php echo $id ?>" name="y1[]" />
		<input type="hidden" id="x2_<?php echo $id ?>" name="x2[]" />
		<input type="hidden" id="y2_<?php echo $id ?>" name="y2[]" />
		<input type="hidden" id="w_<?php echo $id ?>" name="w[]" />
		<input type="hidden" id="h_<?php echo $id ?>" name="h[]" />
		<input type="hidden" id="w_div_<?php echo $id ?>" name="w_div[]"/>
		<input type="hidden" id="h_div_<?php echo $id ?>" name="h_div[]"/>
</div>

<?php if(!empty($init)): ?>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.Jcrop.min.js';?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/jquery.Jcrop.min.css';?>">
<script type="text/javascript">

var jcrop_api = new Array();
function add_namepath(input,id){
		console.log(id)
    //prepare HTML5 FileReader
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        	var image = new Image();
        	var width;
        	var height;
        	image.src =  e.target.result;
        	image.onload = function(){
					width = this.width;
					height = this.height;
        	 	//console.log(width+' '+height);	                	 
            	if( input.files[0].size >5000*1000 || width >3092 || height >3000){
            		$("#"+id).val('');
            		$('#preview_'+id).attr('src', '<?php echo base_url().'images/thumbnail/thumb_box.jpg';?>');
            		// $('#preview_'+id).attr('width', '<?php echo $width;?>');
            		// $('#preview_'+id).attr('height', '<?php echo $height;?>');
            		alert('Sorry, your file is too large.');
            	}else{
            		if(jcrop_api[id]){
            			jcrop_api[id].destroy();
            			$('#preview_'+id).removeAttr('style');
		                jcrop_api[id] = null;
            		}
            		
            		var temp = $("#"+id).val();
					$('#preview_'+id).attr('src', e.target.result);
					$('#photo_name_'+id).val(input.files[0].name);
					$('#photo_path_'+id).val(input.files[0].name);
					$('#photo_size_'+id).val("TRUE");
					$('#preview_'+id).load(function(){
						
						var ration = <?php  echo (int)$width?> / <?php echo(int)$height; ?>;
						if(id == 'mobile'){
							var ration = 0.94;
						}
						
					    $('#preview_'+id).Jcrop({
					    	setSelect:   [ 0, 0, 50, 50 ],
					        minSize: [50, 50], // min crop size
					        aspectRatio : ration, // keep aspect ratio 1:1
					        bgFade: false, // use fade effect
					        bgOpacity: .3, // fade opacity
					        onChange: function (c) {											
											updateInfo(c,id);
									},
									onSelect: function (c) {
						        updateInfo(c,id);
									},
									onRelease: function (c) {
						        clearInfo(id);
									},
					    }, function (){
					    	jcrop_api[id] = this;
					    	$('#w_div_'+id).val( this.ui.holder[0]['clientWidth'] );
					    	$('#h_div_'+id).val( this.ui.holder[0]['clientHeight'] );
					    });
					});
            	}
             };
        };
        reader.readAsDataURL(input.files[0]);
	}
}
function updateInfo(e,id) {
    $('#x1_'+id).val(e.x);
    $('#y1_'+id).val(e.y);
    $('#x2_'+id).val(e.x2);
    $('#y2_'+id).val(e.y2);
    $('#w_'+id).val(e.w);
    $('#h_'+id).val(e.h);		
	// $('html, body').animate({
 //        scrollTop: $("#preview_"+id).offset().top
 //    }, 2000);
}

function clearInfo(id) {
    // $('#w_'+id).val('');
    // $('#h_'+id).val('');
		if($('#w_'+id).val() == 0 || $('#h_'+id).val() == 0 ){			
			 $('#preview_'+id).Jcrop({
			 setSelect:   [ 0, 0, 50, 50 ]});
		}
}

</script>
<?php endif; ?>