<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CMS Management</title>
	    	<script>
          var base_url = '<?php echo base_url(); ?>'; 
          var site_url = '<?php echo site_url(); ?>';      
        </script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>

        <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
        <style>
          .bmd-layout-canvas {
            min-height: 100vh;
          }
        </style>
    </head>
    <body>
        <!-- Header -->
      <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay">
        <header class="bmd-layout-header">
          <div class="navbar navbar-light bg-faded">
            <button class="navbar-toggler" type="button" data-toggle="drawer" data-target="#dw-s2">
              <span class="sr-only">Toggle drawer</span>
              <i class="material-icons">menu</i>
            </button>
            <!-- <ul class="nav navbar-nav">
              <li class="nav-item">Title</li>
            </ul> -->
          </div>
        </header>
        <div id="dw-s2" class="bmd-layout-drawer bg-faded">
          <header>
            <a class="navbar-brand">Menu</a>
          </header>
          <ul class="list-group">
            <a class="list-group-item" href="<?php echo site_url('cms/edit');?>">Edit Profile</a>
          </ul>
        </div>